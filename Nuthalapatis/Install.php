<?php

namespace Nuthalapatis;
use Composer\Script\Event;

class Install
{
	/**
	 * symlinks for wp-config files
	 */
	public static function configSymlinks (Event $event)
	{
		$rootDir = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR;
		$configDir = $rootDir . 'config' . DIRECTORY_SEPARATOR;
		$wordpressDir = $rootDir . 'wp' . DIRECTORY_SEPARATOR;
 
    	//wordpress config file
    	if( !file_exists($wordpressDir . 'wp-config.php') ) {
    		symlink($configDir . 'wp-config.php', $wordpressDir);
    	}

    	//wordpress config default file
    	if( !file_exists($wordpressDir . 'wp-config.default.php') ) {
    		symlink($configDir . 'wp-config.default.php', $wordpressDir);
    	}

    	//wordpress config environment file
    	if( !file_exists($wordpressDir . 'wp-config.env.php') ) {
    		symlink($configDir . 'wp-config.env.php', $wordpressDir);
    	}

    	//wordpress config file - Development
    	if( !file_exists($wordpressDir . 'wp-config.development.php') ) {
    		symlink($configDir . 'wp-config.development.php', $wordpressDir);
    	}

    	//wordpress config default file - Staging
    	if( !file_exists($wordpressDir . 'wp-config.staging.php') ) {
    		symlink($configDir . 'wp-config.staging.php', $wordpressDir);
    	}

    	//wordpress config environment file - Production
    	if( !file_exists($wordpressDir . 'wp-config.production.php') ) {
    		symlink($configDir . 'wp-config.production.php', $wordpressDir);
    	}
	}
}

?>