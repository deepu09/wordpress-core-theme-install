// Project paths
var pkgJson		= require('./package.json'),
    wpRoot      = 'proj',
	project 	= wpRoot + '/wp-content/themes/' + pkgJson.name + '/',
	assets 		= project + 'assets/',
	src 		= assets + 'src/',
	build 		= assets + 'build/',
	composer 	= assets + 'vendor/',
	bootstrap 	= composer + 'bootstrap-sass/assets',
	gulp		= require('gulp'),
	vfs         = require('vinyl-fs'),
	plugins 	= require('gulp-load-plugins')({ camelize: true }),
	config = {
		src: {
			js: src + 'js/*.js',
			images: src + 'images',
			sass: src + 'sass/*.scss'
		},
		build : {
			js: build + 'js/',
            css: build + 'css/',
			images: build + 'images/'
		}
	},
	sassOptions = {
		errLogToConsole: true,
		outputStyle: 'expanded'
	},
	autoprefixerOptions = {
		browsers: [
			'last 2 version',
			"Android 2.3",
			"Android >= 4",
			"Chrome >= 20",
			"Firefox >= 24",
			"Explorer >= 8",
			"iOS >= 6",
			"Opera >= 12",
			"Safari >= 6"
		]
	};


/*================================================================
 Symlinks
 ================================================================*/
gulp.task('symlinks', function() {
    return vfs.src('config/*.php', {followSymlinks: false})
        .pipe(vfs.symlink(wpRoot));
});


/*================================================================
 SASS to CSS
 ================================================================*/
gulp.task('styles', function() {
	return gulp.src(config.src.sass)
		.pipe(plugins.sass(sassOptions).on('error', plugins.sass.logError))
		.pipe(plugins.autoprefixer(autoprefixerOptions))
		.pipe(gulp.dest(config.build.css + 'style.css'))
		.pipe(plugins.notify({ message: 'Styles task complete' }))
});


/*================================================================
 JS
 ================================================================*/
gulp.task('scripts', function() {
	return gulp.src([bootstrap + 'javascripts/bootstrap.js', config.src.js])
		.pipe(plugins.jshint('.jshintrc'))
		.pipe(plugins.jshint.reporter('default'))
		.pipe(plugins.concat('scripts.js'))
		.pipe(gulp.dest(config.build.js))
		.pipe(plugins.uglify())
		.pipe(gulp.dest(config.build.js))
		.pipe(plugins.notify({ message: 'Scripts task complete' }));
});


/*================================================================
 Images
 ================================================================*/
gulp.task('images', function() {
	return gulp.src(config.src.images)
		.pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
		.pipe(gulp.dest(config.build.images))
		.pipe(plugins.notify({ message: 'Images task complete' }));
});


/*================================================================
 Watch
 ================================================================*/
gulp.task('watch', function() {
	// Watch .scss files
	gulp.watch(config.src.sass, ['styles']);

	// Watch .js files
	gulp.watch(config.src.js, ['scripts']);

	// Watch image files
	gulp.watch(config.src.images, ['images']);

});


/*================================================================
 Default Task
 ================================================================*/
gulp.task('default', ['symlinks', 'styles', 'scripts', 'images']);

